package com.dynamicsmachines.testgooglemapsweb;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dynamicsmachines.googlemapsweb.GoogleMapsWeb;
import com.dynamicsmachines.googlemapsweb.GoogleMapsWebCallback;

public class MainActivity extends AppCompatActivity {

    EditText latStart,lngStart;
    EditText latEnd,lngEnd;
    TextView txtTime,txtDistance,txtRoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize ui elements
        latStart = findViewById(R.id.txtLatStart);
        lngStart = findViewById(R.id.txtLngStart);
        latEnd = findViewById(R.id.txtLatEnd);
        lngEnd = findViewById(R.id.txtLngEnd);
        txtTime = findViewById(R.id.txtTime);
        txtDistance = findViewById(R.id.txtDistance);
        txtRoad = findViewById(R.id.txtRoad);

    }

    // called when the button is pressed
    public void getLocation (View v)
    {
        // make sure all data is filled in UI
        if (latStart.getText().toString().isEmpty()
                || latStart.getText().toString().isEmpty()
                || latStart.getText().toString().isEmpty()
                || latStart.getText().toString().isEmpty())
        {
            Toast.makeText(this,"Please fill all locations",Toast.LENGTH_LONG).show();
            return;
        }

        // create class object
        GoogleMapsWeb mapsWeb = new GoogleMapsWeb(this,findViewById(R.id.mainlayout));

        // pass the start and end locations
        mapsWeb.setStartEndLocation(latStart.getText().toString(),
                lngStart.getText().toString(),
                latEnd.getText().toString(),
                lngEnd.getText().toString());

        // start parsing the location, callback function will be called when done
        mapsWeb.parseRoutes(new GoogleMapsWebCallback() {
            @Override
            public void onComplete(boolean valid, String time, String distance, String road) {
                // easy way to change UI element from another thread
                txtTime.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txtTime.setText(time);
                    }
                },100);
                txtDistance.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txtDistance.setText(distance);
                    }
                },100);
                txtRoad.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txtRoad.setText(road);
                    }
                },100);
            }
        });
    }
}